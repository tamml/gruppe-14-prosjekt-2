#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
from pybricks.messaging import BluetoothMailboxClient, TextMailbox


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

# Joystick motors
joystickPitchMotor = Motor(Port.D)
joystickYawMotor = Motor(Port.C)

# clawButton
clawButton = TouchSensor(Port.S1)

# Write your program here.


SERVER = 'ev3dev'

client = BluetoothMailboxClient()
outData = TextMailbox('controllerInput', client)

ev3.screen.print('establishing connection...')
client.connect(SERVER)
ev3.screen.print('connected!')
ev3.speaker.beep()

while True:
    # Get information from claw, pitch and yaw on the controller
    claw_button_pressed = int(clawButton.pressed())
    pitch_angle = joystickPitchMotor.angle()
    yaw_angle = joystickYawMotor.angle()

    # Send the data to the serverEv3

    ## The Data combined as a string to minimize the amout of data beeing sendt 
    ## awell as removing the issue of overlapping data

    dataString = str(claw_button_pressed)+";"+str(pitch_angle)+";"+str(yaw_angle)
    outData.send(dataString)
    

        
    
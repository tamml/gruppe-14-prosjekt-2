#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
from pybricks.messaging import BluetoothMailboxServer, TextMailbox


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

# # Motors to move the arm
PitchMotor = Motor(Port.A)
YawMotor = Motor(Port.D)

# # clawMotor
clawMotor = Motor(Port.B)


# Write your program here.

server = BluetoothMailboxServer()
inData = TextMailbox('controllerInput', server)

# The server must be started before the client!
ev3.screen.print('waiting for connection...')
server.wait_for_connection()
ev3.screen.print('connected!')
ev3.speaker.beep()


# joystick deadzone
joystickDeadzone = 10

# Speed of the arm
motorSpeed = 450
# Speed of the claw
clawMotorSpeed = 400


clawMotor.run_time(-100, 1500, Stop.HOLD, True)
clawMotor.hold()
clawMotor.reset_angle(0)
clawMotor.run_target(clawMotorSpeed, 55, Stop.HOLD, False)


def deCode(inputData):
    
    data = inputData.split(";")

    clawClose = bool(int(data[0]))
    pitch = int(data[1])
    yaw = int(data[2])

    return clawClose, pitch, yaw

# Run Loop
while True:

    inputData = inData.read()
    
    clawClose, pitch, yaw = deCode(inputData)


    # Når knappen trykkes lukkes cloen og åpnes når man slipper knappen
    if(clawClose):
        # ev3.screen.print("Closing")

        clawMotor.run_target(clawMotorSpeed,10, Stop.HOLD, False)
    else:

        # ev3.screen.print("Opening")
        clawMotor.run_target(clawMotorSpeed, 90, Stop.HOLD, False)

    # ev3.screen.print(pitch)
    # Når josytikken bevegs skal armen også bevege seg
    if(abs(pitch) > joystickDeadzone):

        # framover joystick som beveger armen oppover
        if(pitch > 0):
            
            PitchMotor.run(-motorSpeed*abs(pitch)/55)

        # bakover joystick som beveger armen nedover
        elif(pitch < 0):
            PitchMotor.run(motorSpeed*abs(pitch)/40)
    else:
        # holder motoren i posisjon når den ikke skal beveges
        PitchMotor.hold()

    if(abs(yaw) > joystickDeadzone):

        # sidelengs joystick som roterer armen
        if(yaw > 0):
            
            YawMotor.run(-motorSpeed/5*abs(yaw)/55)

        # sidelengs joystick som beveger roterer armen 
        elif(yaw < 0):
            YawMotor.run(motorSpeed/5*abs(yaw)/40)
    else:
        # holder motoren i posisjon når den ikke skal beveges
        YawMotor.hold()

